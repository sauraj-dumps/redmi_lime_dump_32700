## qssi-user 11 RKQ1.201004.002 V12.5.8.0.RJQMIXM release-keys
- Manufacturer: xiaomi
- Platform: bengal
- Codename: lime
- Brand: Redmi
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.201004.002
- Incremental: V12.5.8.0.RJQMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/lime/lime:11/RKQ1.201004.002/V12.5.8.0.RJQMIXM:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.201004.002-V12.5.8.0.RJQMIXM-release-keys
- Repo: redmi_lime_dump_32700


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
